package com.humanbooster.twitter.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Ben on 27/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Data
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class UserProfile implements Serializable {

    private Long id;
    private String nickName;
    private String email;
    private String firstName;
    private String lastName;
    private String profilePicture;
    private Date subscriptionDate;
    private Integer tweetsNb;
    private Integer likedTweets;
}
