package com.humanbooster.twitter.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Ben on 28/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Data
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class TimelineTweet implements Serializable {

    private Long id;
    private String userNickName;
    private String userImage;
    private String text;
    private String image;
    private Date postDate;
    private Integer likes;
    private Boolean liked;
}
