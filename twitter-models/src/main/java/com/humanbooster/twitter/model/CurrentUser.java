package com.humanbooster.twitter.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Created by Ben on 26/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Data
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class CurrentUser implements Serializable {

    private Long id;
    private String email;
    private String nickname;

    public CurrentUser(Long id, String email, String nickname) {
        this.id = id;
        this.email = email;
        this.nickname = nickname;
    }
}
