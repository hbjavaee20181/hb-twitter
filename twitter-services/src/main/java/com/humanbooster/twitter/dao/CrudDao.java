package com.humanbooster.twitter.dao;

import java.io.Serializable;

/**
 * Created by Ben on 14/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public interface CrudDao<T, PK extends Serializable> {
    T create(T t);

    T find(PK id);

    T update(T t);

    void delete(T t);
}
