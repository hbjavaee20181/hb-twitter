package com.humanbooster.twitter.dao.impl;

import com.humanbooster.twitter.dao.HashTagDao;
import com.humanbooster.twitter.entity.HashTag;

import javax.ejb.Stateless;

/**
 * Created by Ben on 26/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class JpaHashtagDao extends JpaCrudDao<HashTag, Long> implements HashTagDao {

    @Override
    protected Class<HashTag> getTargetClass() {
        return HashTag.class;
    }
}
