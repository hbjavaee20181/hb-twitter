package com.humanbooster.twitter.dao.impl;

import com.humanbooster.twitter.dao.NotificationDao;
import com.humanbooster.twitter.entity.Notification;

import javax.ejb.Stateless;

/**
 * Created by Ben on 26/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class JpaNotificationDao extends JpaCrudDao<Notification, Long> implements NotificationDao {

    @Override
    protected Class<Notification> getTargetClass() {
        return Notification.class;
    }
}
