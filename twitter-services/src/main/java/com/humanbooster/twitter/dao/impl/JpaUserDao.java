package com.humanbooster.twitter.dao.impl;

import com.humanbooster.twitter.dao.UserDao;
import com.humanbooster.twitter.entity.User;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 * Created by Ben on 26/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class JpaUserDao extends JpaCrudDao<User, Long> implements UserDao {

    @Override
    protected Class<User> getTargetClass() {
        return User.class;
    }

    @Override
    public User findUserByUsername(String username) {
        TypedQuery<User> q = em.createQuery("select u from User as u where u.nickName = :nickName", User.class);
        q.setParameter("nickName", username);
        try {
            return q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public User findUserByEmail(String email) {
        TypedQuery<User> q = em.createQuery("select u from User as u where u.email = :email", User.class);
        q.setParameter("email", email);
        try {
            return q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
