package com.humanbooster.twitter.dao;

import com.humanbooster.twitter.entity.Notification;

import javax.ejb.Local;

/**
 * Created by Ben on 26/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Local
public interface NotificationDao  extends CrudDao<Notification, Long>{
}
