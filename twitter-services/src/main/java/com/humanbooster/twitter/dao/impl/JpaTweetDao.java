package com.humanbooster.twitter.dao.impl;

import com.humanbooster.twitter.dao.TweetDao;
import com.humanbooster.twitter.entity.Tweet;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Ben on 26/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class JpaTweetDao extends JpaCrudDao<Tweet, Long> implements TweetDao {

    @Override
    protected Class<Tweet> getTargetClass() {
        return Tweet.class;
    }


    @Override
    public List<Tweet> getAllTweets() {
        TypedQuery<Tweet> q = em.createQuery("select t from Tweet as t order by t.postDate desc", Tweet.class);
        return q.getResultList();
    }
}
