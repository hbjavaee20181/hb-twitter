package com.humanbooster.twitter.dao;

import com.humanbooster.twitter.entity.Tweet;

import javax.ejb.Local;
import java.util.List;

/**
 * Created by Ben on 26/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Local
public interface TweetDao extends CrudDao<Tweet, Long> {
    List<Tweet> getAllTweets();
}
