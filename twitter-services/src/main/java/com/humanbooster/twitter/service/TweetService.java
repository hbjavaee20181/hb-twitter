package com.humanbooster.twitter.service;

import com.humanbooster.twitter.dao.TweetDao;
import com.humanbooster.twitter.dao.UserDao;
import com.humanbooster.twitter.entity.Tweet;
import com.humanbooster.twitter.entity.User;
import com.humanbooster.twitter.model.TimelineTweet;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Ben on 26/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class TweetService {

    @EJB
    private TweetDao tweetDao;

    @EJB
    private UserDao userDao;

    public List<TimelineTweet> getTimelineTweets(Long userId) {
        //TODO At the moment, we retrieve all tweets ;-), we need also to manaage an offset for the progressive loading
        List<Tweet> allTweets = tweetDao.getAllTweets();
        return allTweets
                .stream()
                .map(t -> {
                    TimelineTweet tt = new TimelineTweet();
                    tt.setId(t.getId());
                    tt.setUserNickName(t.getAuthor().getNickName());
                    tt.setUserImage(t.getAuthor().getProfilePicture());
                    tt.setText(t.getText());
                    tt.setImage(t.getImage());
                    tt.setPostDate(t.getPostDate());
                    tt.setLikes(t.getLikingUsers().size());
                    tt.setLiked(t.getLikingUsers()
                            .stream()
                            .map(User::getId)
                            .collect(Collectors.toList())
                            .contains(userId));
                    return tt;
                })
                .collect(Collectors.toList());
    }

    public void postTweet(Long userId, String text) {
        // Get now and current user
        Date now = new Date();
        User currentUser = userDao.find(userId);

        //Create and fill the tweet
        Tweet newTweet = new Tweet();
        newTweet.setAuthor(currentUser);
        newTweet.setPostDate(now);
        newTweet.setText(text);

        tweetDao.create(newTweet);
    }

    public void likeTweet(Long userId, Long tweetId) {
        // Find the tweet and the user
        User user = userDao.find(userId);
        Tweet tweet = tweetDao.find(tweetId);

        tweet.getLikingUsers().add(user);

        tweetDao.update(tweet);
    }

    public void unlikeTweet(Long userId, Long tweetId) {
        // Find the tweet and the user
        User user = userDao.find(userId);
        Tweet tweet = tweetDao.find(tweetId);

        tweet.getLikingUsers().remove(user);

        tweetDao.update(tweet);
    }
}
