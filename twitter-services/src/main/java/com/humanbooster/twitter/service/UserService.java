package com.humanbooster.twitter.service;

import com.humanbooster.twitter.dao.UserDao;
import com.humanbooster.twitter.entity.User;
import com.humanbooster.twitter.model.CurrentUser;
import com.humanbooster.twitter.model.UserProfile;
import com.humanbooster.twitter.util.HashUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Date;

/**
 * Created by Ben on 26/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Stateless
public class UserService {

    @EJB
    private UserDao userDao;

    public CurrentUser authenticate(String email, String password) {
        String hashedPassword = HashUtils.hashPassword(password);
        User user = userDao.findUserByEmail(email);
        if (user != null && user.getPassword().equals(hashedPassword)) {
            return new CurrentUser(user.getId(), user.getEmail(), user.getNickName());
        }
        return null;
    }

    public Boolean userWithUsernameExists(String username) {
        User user = userDao.findUserByUsername(username);
        return user != null;
    }

    public Boolean userWithEmailExists(String email) {
        User user = userDao.findUserByEmail(email);
        return user != null;
    }

    public void registerUser(String firstName, String lastName, String userName, String email, String password) {

        // Hash the password and create the current date for subscription date
        String hashedPassword = HashUtils.hashPassword(password);
        Date subscriptionDate = new Date();

        // Create and fill the user
        User newUser = new User();
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setNickName(userName);
        newUser.setEmail(email);
        newUser.setPassword(hashedPassword);
        newUser.setSubscriptionDate(subscriptionDate);

        // Persist
        userDao.create(newUser);
    }

    public UserProfile getUserProfile(Long userId) {
        User user = userDao.find(userId);
        UserProfile up = new UserProfile();
        up.setId(user.getId());
        up.setEmail(user.getEmail());
        up.setFirstName(user.getFirstName());
        up.setLastName(user.getLastName());
        up.setNickName(user.getNickName());
        up.setProfilePicture(user.getProfilePicture());
        up.setSubscriptionDate(user.getSubscriptionDate());
        up.setLikedTweets(user.getLikedTweets().size());
        up.setTweetsNb(user.getTweets().size());
        return up;
    }
}
