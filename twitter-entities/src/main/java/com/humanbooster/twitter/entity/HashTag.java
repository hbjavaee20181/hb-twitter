package com.humanbooster.twitter.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Ben on 26/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Entity
@Table(name = "HASHTAGS")
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class HashTag implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "HASHTAG_ID")
    private Long id;

    @Column(name = "HASHTAG_TEXT")
    private String text;

    @ManyToMany(mappedBy = "hashtags")
    private List<Tweet> tweets;
}
