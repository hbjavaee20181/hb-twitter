package com.humanbooster.twitter.web.filter;

import com.humanbooster.twitter.web.bean.UserBean;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Ben on 26/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@WebFilter(urlPatterns = "/app/*")
public class LoginFilter implements Filter {

    private static final String LOGIN_PAGE = "/login.xhtml";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        HttpSession session = req.getSession(false);
        UserBean userBean = (session != null) ? (UserBean) session.getAttribute("userBean") : null;

        if (userBean != null && userBean.isAuthenticated()) {
            chain.doFilter(request, response);
        } else {
            resp.sendRedirect(req.getServletContext().getContextPath() + LOGIN_PAGE);
        }
    }

    @Override
    public void destroy() {

    }
}
