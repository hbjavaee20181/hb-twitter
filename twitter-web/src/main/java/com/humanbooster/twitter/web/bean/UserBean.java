package com.humanbooster.twitter.web.bean;

import com.humanbooster.twitter.model.CurrentUser;
import com.humanbooster.twitter.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 * Created by Ben on 26/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@ManagedBean
@SessionScoped
public class UserBean {

    private static Logger logger = LoggerFactory.getLogger(UserBean.class);

    private static final String LOGOUT_OUTCOME = "/login.xhtml?faces-redirect=true"; // Implicit navigation
    private static final String LOGIN_SUCCESS_OUTCOME = "/app/timeline.xhtml?faces-redirect=true"; // Explicit navigation (see faces-config)

    @EJB
    private UserService userService;

    private CurrentUser currentUser;

    public boolean isAuthenticated() {
        return currentUser != null;
    }

    public String login(String email, String password) {
        CurrentUser currentUser = userService.authenticate(email, password);
        if (currentUser != null) {
            logger.info("User {} successfully authenticated !", email);
            this.currentUser = currentUser;
            return LOGIN_SUCCESS_OUTCOME;
        }
        logger.info("User {} try to authenticate and fail...", email);
        // Display an error message if bad credentials
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage message = new FacesMessage("Bad login or password");
        context.addMessage(null, message);
        return null;
    }

    public String logout() {
        logger.info("User {} logged out ...", currentUser.getEmail());
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return LOGOUT_OUTCOME;
    }

    public CurrentUser getCurrentUser() {
        return currentUser;
    }
}
