package com.humanbooster.twitter.web.utils;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 * Created by Ben on 27/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */
public final class FacesUtils {
    private FacesUtils() {

    }

    public static HttpSession getHttpSession() {
        return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getHttpSessionAttribute(String key) {
        return (T) getHttpSession().getAttribute(key);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getRequestParameter(String key) {
        return (T) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(key);
    }
}