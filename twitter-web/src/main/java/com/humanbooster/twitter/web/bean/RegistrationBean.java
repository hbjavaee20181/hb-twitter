package com.humanbooster.twitter.web.bean;

import com.humanbooster.twitter.service.UserService;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Ben on 26/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@ManagedBean
@RequestScoped
public class RegistrationBean {

    private static final String REGISTRATION_SUCCESS = "login.xhtml?faces-redirect=true";

    @EJB
    private UserService userService;

    @NotNull
    @Size(min = 2, max = 40)
    private String firstName;

    @NotNull
    @Size(min = 2, max = 40)
    private String lastName;

    @NotNull
    @Size(min = 4, max = 20)
    private String nickName;

    @NotNull
    @Size(min = 4)
    private String email;

    @NotNull
    @Size(min = 4, max = 40)
    private String password;

    public String validate() {
        // Get the JSF context
        FacesContext context = FacesContext.getCurrentInstance();

        // Check if a user with the given email and username exists
        Boolean emailExists  = userService.userWithEmailExists(email);
        Boolean userNameExists = userService.userWithUsernameExists(nickName);

        // If exists, redirect and error
        if (userNameExists || emailExists) {
            String detail = userNameExists ? "Error : Username already exists" : "Error : Email already exists";
            FacesMessage message = new FacesMessage(detail);
            context.addMessage(null, message);
            return null;
        }

        userService.registerUser(firstName, lastName, nickName, email, password);

        return REGISTRATION_SUCCESS;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
