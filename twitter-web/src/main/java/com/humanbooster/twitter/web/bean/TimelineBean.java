package com.humanbooster.twitter.web.bean;

import com.humanbooster.twitter.model.TimelineTweet;
import com.humanbooster.twitter.model.UserProfile;
import com.humanbooster.twitter.service.TweetService;
import com.humanbooster.twitter.service.UserService;
import com.humanbooster.twitter.web.utils.FacesUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.util.List;

/**
 * Created by Ben on 26/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@ManagedBean
@RequestScoped
public class TimelineBean {

    private static Logger logger = LoggerFactory.getLogger(TimelineBean.class);

    @EJB
    private UserService userService;

    @EJB
    private TweetService tweetService;

    public UserProfile getUserProfile() {
        UserBean userBean = FacesUtils.getHttpSessionAttribute("userBean");
        return userService.getUserProfile(userBean.getCurrentUser().getId());
    }

    public void postTweet(String tweet) {
        UserBean userBean = FacesUtils.getHttpSessionAttribute("userBean");
        tweetService.postTweet(userBean.getCurrentUser().getId(), tweet);
    }

    public List<TimelineTweet> getTimelineTweets() {
        UserBean userBean = FacesUtils.getHttpSessionAttribute("userBean");
        return tweetService.getTimelineTweets(userBean.getCurrentUser().getId());
    }

    public void likeTweet() {
        UserBean userBean = FacesUtils.getHttpSessionAttribute("userBean");
        String tweetIdStr = FacesUtils.getRequestParameter("tweetId");
        if (tweetIdStr == null) {
            // Should never happened
            return;
        }
        tweetService.likeTweet(userBean.getCurrentUser().getId(), Long.parseLong(tweetIdStr));
        logger.info("Tweet " + tweetIdStr + " liked !");
    }

    public void unlikeTweet() {
        UserBean userBean = FacesUtils.getHttpSessionAttribute("userBean");
        String tweetIdStr = FacesUtils.getRequestParameter("tweetId");
        if (tweetIdStr == null) {
            // Should never happened
            return;
        }
        tweetService.unlikeTweet(userBean.getCurrentUser().getId(), Long.parseLong(tweetIdStr));
        logger.info("Tweet " + tweetIdStr + " unliked !");
    }
}
